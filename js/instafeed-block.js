/**
 * @file
 * Logic to initialize Instafeed.js with module settings.
 */
(function (Drupal, drupalSettings) {
  Drupal.behaviors.instafeedBlock = {
    attach: function (context, settings) {
      // Element to display the feed in.
      const instafeedId = document.getElementById('instafeed');

      if (instafeedId && !instafeedId.dataset.instafeedInitialized) {
        // Set a flag to avoid re-initialization.
        instafeedId.dataset.instafeedInitialized = true;

        // Get block form settings.
        const template = drupalSettings.instafeed_block.template;
        const limit = parseInt(drupalSettings.instafeed_block.limit);
        const filterObject = drupalSettings.instafeed_block.filters || {};
        const filters = Object.keys(filterObject);
        const accessToken = drupalSettings.instafeed_block.accessToken;

        // Since the instafeed library limits the number of posts before
        // we can filter the posts by media type, we fetch an additional number
        // of posts to come closer to the requested limit.
        // A higher limit affects the load time, hence only a few additional.
        const fetchPosts = limit + 20;
        // Counter to keep track of the number of found posts for requested
        // media types.
        let foundPosts = 0;

        // Initialize Instafeed.js.
        const feed = new Instafeed({
          limit: fetchPosts,
          template: template,
          accessToken: accessToken,
          // Filter the feed based on the requested type of media.
          filter: function(image) {
            if (filters.length > 0) {
              if (foundPosts < limit) {
                if (filters.includes('image') && image.type === 'image') {
                  foundPosts++;
                  return true;
                }
                if (filters.includes('video') && image.type === 'video') {
                  foundPosts++;
                  return true;
                }
                if (filters.includes('album') && image.type === 'album') {
                  foundPosts++;
                  return true;
                }
                // Fallback if no filter matches. We should never reach this point.
                if (filters.length === 0) {
                  foundPosts++;
                  return true;
                }
              }
              // Requested limit reached.
              else {
                return false;
              }
            }
            // Display all media if no filters are set.
            else {
              if (foundPosts < limit) {
                foundPosts++;
                return true;
              }
              // Requested limit reached.
              else {
                return false;
              }
            }
          },
          // Element id to display the feed in.
          target: 'instafeed'
        });
        feed.run();
      }
    }
  };
})(Drupal, drupalSettings);
