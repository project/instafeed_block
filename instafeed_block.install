<?php

/**
 * @file
 * Install, update and uninstall functions.
 */

/**
 * Implements hook_install().
 */
function instafeed_block_install() {
  if (!instafeed_block_library_exists()) {
    \Drupal::messenger()->addWarning(t('The instafeed.js library needs to be <a href="@url">downloaded</a> and extracted into the libraries folder. Make sure the installation path for the module required file is /libraries/instafeed-js/dist/instafeed.min.js', ['@url' => 'https://github.com/stevenschobert/instafeed.js/archive/refs/tags/v2.0.0.zip']));
  }
}

/**
 * Implements hook_requirements().
 */
function instafeed_block_requirements($phase) {
  if ($phase != 'runtime') {
    return [];
  }

  $library_exists = instafeed_block_library_exists();

  $library_requirements = [
    'instafeed_library_downloaded' => [
      'title' => t('instafeed.js library'),
      'value' => $library_exists ? t('Installed') : t('Not installed'),
      'description' => $library_exists ? '' : t('The instafeed.js library needs to be <a href="@url">downloaded</a> and extracted into the libraries folder. Make sure the installation path for the module required file is /libraries/instafeed-js/dist/instafeed.min.js', ['@url' => 'https://github.com/stevenschobert/instafeed.js/archive/refs/tags/v2.0.0.zip']),
      'severity' => $library_exists ? REQUIREMENT_OK : REQUIREMENT_ERROR,
    ],
  ];

  return $library_requirements;
}

/**
 * Function to check if the instafeed.js library exists.
 *
 * @return bool
 *   TRUE if the library exists, FALSE otherwise.
 */
function instafeed_block_library_exists() {
  $library = \Drupal::service('library.discovery')->getLibraryByName('instafeed_block', 'instafeed.js');
  $library_exists = !empty($library['js'][0]['data']) &&
    file_exists(DRUPAL_ROOT . '/' . $library['js'][0]['data']);

  return $library_exists ? TRUE : FALSE;
}

/**
 * Implements hook_uninstall().
 */
function instafeed_block_uninstall() {
  // Delete state values associated with the module.
  \Drupal::state()->delete('instafeed_block.access_token');
  \Drupal::state()->delete('instafeed_block.token_expiration');
  \Drupal::state()->delete('instafeed_block.last_execution');

  \Drupal::messenger()->addWarning(t('You may also want to remove the instafeed.js library if it is not used anymore: /libraries/instafeed-js'));
}
